; Is it possible to be super addicted to a keyboard shortcut? This is so useful!
;
; Licensed under the Unlicense. See LICENSE

while 1 > 0
{
	If (GetKeyState("Pause", "P") && GetKeyState("Down", "P"))
	{
		Send {Media_Play_Pause down}
		Send {Media_Play_Pause up}
	}
	If (GetKeyState("Pause", "P") && GetKeyState("Left", "P"))
	{
		Send {Media_Prev down}
		Send {Media_Prev up}
	}
	If (GetKeyState("Pause", "P") && GetKeyState("Right", "P"))
	{
		Send {Media_Next down}
		Send {Media_Next up}
	}
	If (GetKeyState("Pause", "P") && GetKeyState("PgUp", "P"))
	{
		Send {Volume_Up down}
		Send {Volume_Up up}
	}
	If (GetKeyState("Pause", "P") && GetKeyState("PgDn", "P"))
	{
		Send {Volume_Down down}
		Send {Volume_Down up}
	}
	If (GetKeyState("Pause", "P") && GetKeyState("End", "P"))
	{
		Send {Volume_Mute down}
		Send {Volume_Mute up}
	}
}