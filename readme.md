#Uses for various files:


###fanceh-playpause.ahk

It's both a shame that fb2k hates arbitrary modifiers, and that my IBM Model M doesn't have enough useless keys.
This script solves the former, and makes <kbd>Pause</kbd> + <kbd>Left or Right</kbd> control track forward and back,
and <kbd>Pause</kbd> + <kbd>Down</kbd> play/pause.

###1500x0101.lua

I made this script to generate a bunch of random and mostly unbiased binary. 
It's currently used in the digisheep banner, and eventually the logo