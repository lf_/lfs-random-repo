SendMode Input
~Pause & PgUp:: Send {Volume_Up}
~Pause & PgDn:: Send {Volume_Down}
~Pause & Down:: Send {Media_Play_Pause}
~Pause & Right:: Send {Media_Next}
~Pause & Left:: Send {Media_Prev}